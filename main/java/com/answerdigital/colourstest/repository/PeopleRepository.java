package com.answerdigital.colourstest.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Repository;

import com.answerdigital.colourstest.dto.PersonUpdateDTO;
import com.answerdigital.colourstest.model.Person;

@Repository
public interface PeopleRepository extends JpaRepository<Person, Long> {

	ResponseEntity<Person> save(PersonUpdateDTO person);

	//Person save(PersonUpdateDTO personUpdate);
//
	//ResponseEntity<Person> save(PersonUpdateDTO personUpdate);

	

	//ResponseEntity<Person> save(PersonUpdateDTO personUpdate);

	
}
