package com.answerdigital.colourstest.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import com.answerdigital.colourstest.model.Person;
import com.answerdigital.colourstest.repository.PeopleRepository;

@Service
public class PeopleService {

	@Autowired
	public PeopleRepository peopleRepo;
	
	public ResponseEntity<List<Person>> getPeople(){
		
		List<Person> person = new ArrayList<>();
		peopleRepo.findAll().forEach(person::add);
		HttpHeaders header = new HttpHeaders();
		
		if(person.size()==0) {
			return new ResponseEntity<List<Person>>(new ArrayList<Person>(), header, HttpStatus.NOT_FOUND);
		}
		else {
			return new ResponseEntity<List<Person>>(person, header, HttpStatus.OK);
		}
	}
	
	 public ResponseEntity<Person> getPerson(long id){
		 HttpHeaders header = new HttpHeaders();
		
		 Person person=null;
		 Optional<Person> optionalPerson = peopleRepo.findById(id);
		
		 if(optionalPerson.isPresent()) {
			 person=optionalPerson.get();
		
		 return new ResponseEntity<Person> (person, header, HttpStatus.OK);
		 }
		 else {
			 return new ResponseEntity<Person> (new Person(), header, HttpStatus.NOT_FOUND);
			 
		 }
		 
		 
	 }
	 
	 public ResponseEntity<Person> updatePerson(long id, Person personUpdate){
		 HttpHeaders header = new HttpHeaders();
		 Person person=personUpdate;
		 
		
		 
		 Optional<Person> optionalPerson = peopleRepo.findById(id);
		 if(optionalPerson.isPresent()) {
			 person.setId(id);
		 peopleRepo.save(person);
		 }
		 else {
			 return new ResponseEntity<Person> (header, HttpStatus.NOT_FOUND); 
		 }
		 return new ResponseEntity<Person> (person,header, HttpStatus.OK);
	
	 }
	 
	 public ResponseEntity<Person> addPerson(Person addPerson){
		 
		 HttpHeaders header = new HttpHeaders();
		if(addPerson != null) {
			 peopleRepo.save(addPerson);
			 return new ResponseEntity<Person> (addPerson,header,HttpStatus.OK);
		}else {
			return new ResponseEntity<Person> (header,HttpStatus.NOT_FOUND);
		}
	 }
}
