package com.answerdigital.colourstest.service;

import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import com.answerdigital.colourstest.model.Colour;
import com.answerdigital.colourstest.repository.ColoursRepository;

@Service
public class ColourService {

	@Autowired
	public ColoursRepository colourRepo;
	
	 public ResponseEntity<Colour> getTheColour(long id){
		 HttpHeaders header = new HttpHeaders();
		 Colour colour;
		 Optional<Colour> optionalPerson = colourRepo.findById(id);
		
		 if(optionalPerson.isPresent()) 
		 { colour=optionalPerson.get();
		
		 return new ResponseEntity<Colour> (colour, header, HttpStatus.OK);
		 }
		 else {
			 
			 return new ResponseEntity<Colour> (new Colour(), header, HttpStatus.NOT_FOUND);
		 }
		
	 }
	 
 public ResponseEntity<Colour> addColour(Colour addColour){
		 
		 HttpHeaders header = new HttpHeaders();
		 Colour colour= addColour;
		
		
		 colourRepo.save(colour);
		
			 return new ResponseEntity<Colour> (colour,header,HttpStatus.OK);
		 
	 }


}

